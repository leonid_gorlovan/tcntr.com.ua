<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'seotext_id';
    protected $table = 'easyii_seotext';
}
