<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Text extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'easyii_texts';
    protected $dates = ['deleted_at'];

    public function seo()
    {
        return $this->morphOne(Seo::class, 'item', 'class');
    }
}
