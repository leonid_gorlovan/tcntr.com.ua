<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hcp extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $table = 'app_hcps';
    protected $dates = ['deleted_at'];

    public function seo()
    {
        return $this->morphOne(Seo::class, 'item', 'class');
    }
}
