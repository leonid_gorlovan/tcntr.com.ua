<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Helpers\Translit;
use App\Seo;
use Validator;

class ContentController extends Controller
{
    private $model;

    public function  __construct()
    {
        \App::setLocale('ru');
        $type = request()->route('type');
        $this->model = "\App\\" . studly_case($type);
    }

    public function index(Request $request, $type)
    {
        // $data = $this->model::all();
        // dd($data);

        $isArchive = $request->segment(4) == 'archive' ? 1 : 0;
        return view('admin.content', compact('isArchive', 'type'));
    }

    public function json(Request $request)
    {
        $isArchive = $request->input('isArchive');

        if($isArchive)
        {
            return datatables()->of($this->model::query()->onlyTrashed())->toJson();
        }
        else
        {
            return datatables()->of($this->model::query())->toJson();
        }

    }

    public function form($type, $id = 0)
    {
        $data = $this->model::find($id);
        return view('admin.content_form', compact('type', 'id', 'data'));
    }

    public function save(Request $request, $type, $id = 0)
    {
        $data = $this->model::findOrNew($id);
        $table = $data->getTable();

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|' . Rule::unique($table)->ignore($id, 'id'),
        ]);

        if ($validator->fails()) {
            return redirect('admin/content/' . $type . '/form/' . $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $data = $this->model::findOrNew($id);
        $data->title = $request->input('title');
        $data->text = $request->input('text');
        if($id = 0) $data->slug = Translit::slug($request->input('title'));
        $data->save();

        $seo = $data->seo;
        if(empty($seo)) $seo = new Seo;

        $seo->h1 = $request->input('seo.h1');
        $seo->title = $request->input('seo.title');
        $seo->keywords = $request->input('seo.keywords');
        $seo->description = $request->input('seo.description');
        $data->seo()->save($seo);

        return redirect('admin/content/' . $type . '/form/' . $data->id)->with(['success' => 'Сохранено']);
    }

    public function delete(Request $request, $type, $id = 0)
    {
        $data = $this->model::find($id);

        if(!empty($data))
        {
            $data->delete();
        }

        return redirect('admin/content/' . $type)->with(['success' => 'Отправлено в Архив']);
    }

    public function restore($type, $id)
    {
        $data = $this->model::withTrashed()->find($id);

        if(!empty($data))
        {
            $data->restore();
        }

        return redirect('/admin/content/' . $type)->with(['success' => 'Восстановлено']);
    }
}
