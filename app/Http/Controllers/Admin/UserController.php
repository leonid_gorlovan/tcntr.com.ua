<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;

class UserController extends Controller
{
    public function  __construct()
    {
        \App::setLocale('ru');
    }

    public function login()
    {
        return view('vendor.adminlte.login');
    }

    public function auth(Request $request)
    {
        $rule['password'] = 'required|min:6|max:64';
        $rule['email'] = 'required|email';

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect('admin/login')
                        ->withErrors($validator)
                        ->withInput();
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->input('remember')))
        {
            return redirect()->intended('/admin');
        }

        return redirect('admin/login')->with(['warning' => 'Не удалось авторизоваться, проверьте email и пароль']);
    }

    public function index(Request $request, $type)
    {
        $isArchive = $request->segment(4) == 'archive' ? 1 : 0;
        return view('admin.user', compact('isArchive', 'type'));
    }

    public function json(Request $request, $type)
    {
        $isArchive = $request->input('isArchive');
        $user = User::query()->where('role', $type);

        if($isArchive)
        {
            $user = $user->onlyTrashed();
        }

        return datatables()->of($user)->toJson();
    }

    public function form($type, $id = 0)
    {
        $data = User::find($id);
        return view('admin.user_form', compact('type', 'id', 'data'));
    }

    public function save(Request $request, $type, $id = 0)
    {
        $rule['name'] = 'required|max:255';
        $rule['email'] = 'required|email|' . Rule::unique('easyii_users')->ignore($id, 'id');

        if($id == 0)
        {
            $rule['password'] = 'required|min:6|max:255|confirmed';
        }
        else
        {
            $rule['password'] = 'confirmed';
        }

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect('admin/users/' . $type . '/form/' . $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::saveData($request, $type, $id);

        return redirect('admin/users/' . $type . '/form/' . $user->id)->with(['success' => 'Сохранено']);
    }

    public function delete(Request $request, $type, $id = 0)
    {
        $data = User::find($id);

        if(!empty($data))
        {
            $data->delete();
        }

        return redirect('admin/users/' . $type)->with(['success' => 'Отправлено в Архив']);
    }

    public function restore($type, $id)
    {
        $data = User::withTrashed()->find($id);

        if(!empty($data))
        {
            $data->restore();
        }

        return redirect('/admin/users/' . $type)->with(['success' => 'Восстановлено']);
    }
}
