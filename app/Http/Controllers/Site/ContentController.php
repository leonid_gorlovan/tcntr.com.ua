<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\FeedbackMail;
use App\Pages;
use App\Text;
use App\Hcp;
use Validator;

class ContentController extends Controller
{
    public function __constructor()
    {
        \App::setLocale('ua');
    }

    public function index()
    {
        $data = Text::whereSlug('hcp-slider-index')->first();
        return view('site.main', compact('data'));
    }

    public function page($slug)
    {
        $type = 'page';
        $data = Pages::whereSlug($slug)->first();
        return view('site.content', compact('data', 'slug', 'type'));
    }

    public function hcp($slug)
    {
        $type = 'hcp';

        if($slug == 'slider')
        {
            $data = Text::whereSlug($slug)->first();
        }
        else
        {
            $data = Hcp::whereSlug($slug)->first();
        }

        return view('site.content', compact('data', 'slug', 'type'));
    }

    public function sendFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'last_name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'city' => 'required|max:255',
            'email'  => 'required|email',
            'message' => 'required|max:1000',
            'zgoda' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/page/contacts')
                        ->withErrors($validator)
                        ->withInput();
        }

        \Mail::to(env('MAIL_TO_ADDRESS'))->send(new FeedbackMail());
        return redirect('/page/contacts')->with(['success' => 'Сообщение отправлено']);
    }
}
