<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AuthenticateMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(empty($user) || object_get($user, 'role') != 'admin')
        {
        return redirect('login');
        }
        else
        {
            return $next($request);
        }
    }
}
