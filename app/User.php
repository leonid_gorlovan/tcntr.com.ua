<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    public $timestamps = false;
    protected $table = 'easyii_users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function saveData($request, $type, $id)
    {
        $password = $request->input('password');

        $data = self::findOrNew($id);
        $data->role = $type;
        $data->name = $request->input('name');
        $data->email = $request->input('email');

        if(!empty($password))
        {
            $data->password = \Hash::make($password);
        }

        $data->save();

        return $data;
    }
}
