$(document).ready( function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    setTimeout(function() {
		$('.alerts-box').fadeOut();
	}, 5000);

	$(document).on('click', '.alert, .alerts-box', function() {
		$('.alerts-box').fadeOut();
	});

    $('select').select2();

    $(document).on('click', '.btn-archive', function() {
        return confirm('Вы действительно хотите переместить в архив запись ?');
    });

});
