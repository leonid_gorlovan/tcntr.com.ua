@extends('site.tmpl')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 inner-container">
            <img class="slider" src="/images/Branching_Page.png" alt="">
            <div class="richtext">
                <h2>Тецентрик<sup>®</sup><br>ІМУНОТЕРАПІЯ РАКУ</h2>
                <div class="disease blue">
                    <a href="/hcp/slider" style="max-width:390px;"><img src="/images/lung_icon_dktp.png" alt=""></a>
                    <p style="max-width: 390px;">
                        <span style=""><strong><a href="/hcp/slider" style="max-width:390px;">MEТАСТАТИЧНИЙ <br>НЕДРІБНОКЛІТИННИЙ РАК ЛЕГЕНІ</a></strong></span>
                    </p>
                    <a href="/hcp/sliders"></a>
                </div>
                <div class="disease yellow">
                    <a href="/hcp/urothelial-carcinoma" style="max-width:390px;"><img src="/images/bladder_icon_dktp.png" alt="">
                    <p><strong>УРОТЕЛІАЛЬНА КАРЦИНОМА</strong></p></a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 related">
            <div class="blue-border">
                <a href="/page/video"><img src="/video/video.jpg" alt=""><h4>Механізм діЇ препарату ТЕЦЕНТРИК<sup>®</sup></h4>
                <p>Анімаційний 3D-ролик, що пояснює механізм дії препарату</p></a>
            </div>
        </div>

        <div class="col-lg-4 related">
            <div class="blue-border">
                <a href="/page/support" style="height: 157px;"><img src="/images/TECENTRIQNSCLCWeb_CopayCTA-1-1200-Painter.jpg" alt=""><h4>Програма підтримки пацієнтів</h4>
                <p>Програма підтримки пацієнтів, яким призначено лікування препаратом Тецентрик<strong>®</strong> (атезолізумабом)</p></a>
            </div>
        </div>

        <div class="col-lg-4 related">
            <div class="yellow-border">
                <a href="/hcp/nsclc-tecentriq-moa" style="height: 157px;"><img src="/images/articles/tecentriq.jpg" alt=""><h4>Механізм дії Тецентрик®</h4>
                    <p>Тецентрик® націлений на ліганд PD-L1 та сприяє активації протипухлинної імунної відповіді
                        <sup>1</sup></p>
                </a>
                <a href="/hcp/nsclc-tecentriq-moa" style="height: 157px;"></a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 content related-header">
            {!! object_get($data, 'text') !!}
        </div>
    </div>
</div>

@endsection
