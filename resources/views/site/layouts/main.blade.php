    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>{{ $title or null }}</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="/frontEnd/css/styles.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    </head>
    <body>

    Content

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="nav navbar-expand-lg navbar-expand-md">
                        <ul class="nav">
                            <li class="nav-item"><a class="nav-link" href="/page/contacts">Зв’язок з нами</a></li>
                            {{-- <li class="nav-item"><a class="nav-link" href="/page/sitemap">Карта сайту</a></li> --}}
                            <li class="nav-item"><a class="nav-link" href="/page/privacy-policy">Політика
                                    конфіденційності</a></li>
                            <li class="nav-item"><a class="nav-link" href="/page/terms-conditions">Правила та умови</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="http://www.roche.ua/">Рош Україна</a>
                        </ul>
                    </nav>
                    <div class="disclaimer">
                       <p>Інформацію наведено у скороченому вигляді. Більш детальна інформація щодо медичного застосування лікарського засобу Тецентрик® міститься в інструкції (РП МОЗ України UA №/15872/01/01, Наказ №295 від 23.03.2017. Зміни внесено наказом МОЗ України № 123 від 24.01.2018)</p>
                       <p>Сайт http://rapaport.pp.ua/ є власністю ТОВ «Рош Україна». Несанкціоноване копіювання і розповсюдження заборонено.
Правова угода © 201_ ТОВ "Рош Україна"</p>
                        <p>UA/TCN/1807/0012
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="modal fade" id="exampleModalLong" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLongTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #64184f">
                    <h5 class="modal-title" id="exampleModalLongTitle" style="color: #fff;">Застереження</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    >>>> patient-popup
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="accept_medic">Підтверджую</button>
                </div>
            </div>
        </div>
    </div>


    @php
        $current_page = null;
    @endphp

    @if($current_page == '/page/support')
        <div class="modal fade" id="exampleModalLong2" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLongTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #64184f">
                    <h5 class="modal-title" id="exampleModalLongTitle" style="color: #fff;">Застереження</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Вся інформація про лікарські засоби розміщена з метою загального інформування і не є рекламою. Інформація призначена для медичних і фармацевтичних працівників. Підтвердіть, будь-ласка, що ви є спеціалістом в галузі охорони здоров’я/ фармацевтом/ медичним співробітником   або пацієнтом, якому лікар призначив препарат Тецентрик (р) ТОВ «Рош Україна
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="accept_patient">Підтверджую</button>
                </div>
            </div>
        </div>
        </div>
    @endif

    <div class="modal" tabindex="-1" role="dialog"  id="open_link">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Підтвердіть свої наміри</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Ви переходите на ресурс, який не належить та не контролюється ТОВ "Рош - Україна"</p>
          </div>
          <div class="modal-footer">
            <a href="javascript:void(0)" type="button" class="btn btn-primary success_link" >Перейти</a>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
          </div>
        </div>
      </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script src="/frontEnd/js/jquery.cookie.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="/frontEnd/js/user.js"></script>
    </body>
    </html>
