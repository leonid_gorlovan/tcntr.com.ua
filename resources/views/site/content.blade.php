@extends('site.tmpl')
@section('content')

{{--
@if(!empty($menu))
<div class="container-fluid top-menu  hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 top-tabs">
                <ul class="nav">
                    @foreach ($menu as $k => $item)
                        <li class="nav-item ">
                            <a class="nav-link  {{ ($k == 1 && $active) ? 'active' : '' }}"
                                href="{{ $item['link'] }}"
                                title="{{ $item['name'] }}">{{ $item['name'] }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endif
 --}}

@if($type == 'hcp')
<div class="container-fluid section-menu hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav justify-content-center nav-fill">
                    <li class="nav-item has-dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="" aria-expanded="false">Про ТЕЦЕНТРИК®</a>
                        <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(15px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-tecentriq-moa">Запропонований механізм дії</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-tecentriq-dosing">Дозування</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="" aria-expanded="false">Клінічні дані</a>
                        <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(589px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-clinical-data-efficacy">Ефективність</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-clinical-data-study-design">Дизайн досліджень</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-clinical-data-safety-profile">Профіль безпеки</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/nsclc-clinical-data-manage-adverse-events">Контроль побічних явищ</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<br />
@endif

@if($slug == 'slider')
    @include('site._hcp_nsclc_slider')
@endif

<div class="container">
    <div class="row">
        {!! object_get($data, 'text') !!}
    </div>

    @if($slug == 'contacts')
        @include('site._contact');
    @endif
</div>
@endsection
