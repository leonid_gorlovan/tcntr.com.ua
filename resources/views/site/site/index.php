<?php
use yii\helpers\Url;
use yii\easyii\modules\page\api\Page;
use yii\easyii\modules\text\api\Text;

$asset = \app\assets\AppAsset::register($this);

$page = Page::get('home');
$this->title = $page->seo('title', $page->model->title);
$this->params['keywords'] = $page->seo('keywords');
$this->params['description'] = $page->seo('description');

?>
<div class="container-fluid top-menu  hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 top-tabs">
            <?
                if(!empty($aMenu))
                {
                    ?>
                    <ul class="nav">
                        <?
                            foreach($aMenu as $item)
                            {
                                ?>
                                <li class="nav-item "><a class="nav-link active" href="<?=$item['link']?>" title="<?=$item['name']?>"><?=$item['name']?></a></li>
                                <?
                            }
                        ?>
                    </ul>
                    <?
                }
            ?>
            </div>
        </div>
    </div>
</div>
<div class="container  hidden-sm-down">
    <div class="row">
        <div class="col-lg-3 col-md-3">
            <a href="/">
                <img class="logo" src="images/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-9 col-md-9">
            <div class="safety"><a href="" data-toggle="modal" data-target="#exampleModalLong"><img
                            src="images/safety.png" alt="">safety</a></div>
            <div class="information"><a href="">Prescribing Information</a></div>
        </div>
    </div>
</div>
<div class="container hidden-md-up">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-8">
            <a href="/">
                <img class="logo" src="images/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-4">
            <nav class="navbar navbar-toggleable-md bg-faded pull-right burger">
                <button class="navbar-toggler navbar-toggler-right col-xs-6" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <img src="images/burger-menu.png" alt="">
                    </span>
                </button>
            </nav>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>