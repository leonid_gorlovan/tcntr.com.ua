<?php

use app\modules\hcp\api\Page;
use yii\easyii\modules\text\api\Text;
use yii\helpers\Url;

$asset = \app\assets\AppAsset::register($this);


$page = Page::get($slug);

if ($page->model->page_id === null) {
    Yii::$app->controller->redirect("/", "301");
}
$this->title = $page->seo('title', $page->model->title);
$this->params['keywords'] = $page->seo('keywords');
$this->params['description'] = $page->seo('description');
$active = 0;
$actual_link = explode("/", $_SERVER['REQUEST_URI']);
if (strpos(Url::current(), "/hcp") !== false) {
    $active = 1;
}
?>
<div class="container-fluid top-menu  hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 top-tabs">
                <?
                if (!empty($aMenu)) {
                    ?>
                    <ul class="nav">
                        <?

                        foreach ($aMenu as $k => $item) {
                            if ($item['status'] == 1) {
                                ?>
                                <li class="nav-item ">
                                    <a class="nav-link  <?= ($k == 1 && $active) ? 'active' : '' ?>"
                                       href="<?= $item['link'] ?>"
                                       title="<?= $item['name'] ?>">
                                        <?= $item['name'] ?>
                                    </a>
                                </li>
                            <? } ?>
                            <?
                        }
                        ?>
                    </ul>
                    <?
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="container hidden-sm-down">
    <div class="row">
        <div class="col-lg-3 col-md-3">
            <a href="/">
                <img class="logo" src="/images/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-9 col-md-9">
            <!--<div class="safety"><a href="" data-toggle="modal" data-target="#exampleModalLong"><img
                            src="/images/safety.png" alt="">Безпека</a></div>-->
            <div class="safety"><a href="/page/safety"><img
                            src="/images/safety.png" alt="">Інформація про безпеку</a></div>
            <div class="information "><a href="/page/support">Програма підтримки пацієнтів</a></div>
            <div class="information border-right"><a href="/page/instruction">Інструкція</a></div>
            <?
            if ($slug != "index") {
                ?>
                <?
            }
            ?>


        </div>
    </div>
</div>


<?php
if (isset($actual_link[2]) && $actual_link[2] == "lungs" && $slug != "index") {
    ?>
<div class="container hidden-md-up">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-4 col-8">
            <a href="/">
                <img class="logo" src="/images/logo.png" alt="">
            </a>
        </div>
        <div class="col-lg-9 col-md-8 col-sm-8 col-4">
            <nav class="navbar navbar-toggleable-md bg-faded pull-right burger">
                <button class="navbar-toggler navbar-toggler-right col-xs-6" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <img src="/images/burger-menu.png" alt="">
                    </span>
                </button>
            </nav>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item  active">
                        <a class="nav-link" href="#">Важлива інформація</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-tecentriq-moa">Запропонований механізм дії</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-tecentriq-dosing">Дозування</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-efficacy">Ефективність</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-study-design">Дизайн досліджень</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-safety-profile">Профіль безпеки</a>
                    </li>
                    <li class="nav-item section-menu">
                        <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-manage-adverse-events">Контроль побічних явищ</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid section-menu hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav justify-content-center nav-fill">
                    <li class="nav-item has-dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Про ТЕЦЕНТРИК&reg;</a>
                        <ul class="dropdown-menu ">
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-tecentriq-moa">Запропонований механізм дії</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-tecentriq-dosing">Дозування</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Клінічні дані</a>
                        <ul class="dropdown-menu ">
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-efficacy">Ефективність</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-study-design">Дизайн досліджень</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-safety-profile">Профіль безпеки</a>
                            </li>
                            <li class="nav-item section-menu">
                                <a class="nav-link" href="/hcp/lungs/nsclc-clinical-data-manage-adverse-events">Контроль побічних явищ</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
    <?php
}
?>
<?php
if (isset($actual_link[2]) && $actual_link[2] == "urothelial" && $slug != "index") {
    ?>
    <div class="container hidden-md-up">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4 col-8">
                <a href="/">
                    <img class="logo" src="/images/logo.png" alt="">
                </a>
            </div>
            <div class="col-lg-9 col-md-8 col-sm-8 col-4">
                <nav class="navbar navbar-toggleable-md bg-faded pull-right burger">
                    <button class="navbar-toggler navbar-toggler-right col-xs-6" type="button" data-toggle="collapse"
                            data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <img src="/images/burger-menu.png" alt="">
                    </span>
                    </button>
                </nav>
            </div>
            <div class="col-sm-12 col-xs-12">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item  active">
                            <a class="nav-link" href="#">Важлива інформація</a>
                        </li>
                        <li class="nav-item  active">
                            <a class="nav-link" href="">Звернення до представника</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="">Реєстрація для оновлень</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/hcp/urothelial/uc-identifying-tecentriq-patients">ТЕЦЕНТРИК&reg;
                                Відповідність Пацієнта</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/hcp/urothelial/uc-imvigor-210-efficacy">IMvigor210 ефективність</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/hcp/urothelial/uc-imvigor-210-study">IMvigor210 дизайн навчання</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/hcp/urothelial/uc-safety-profile">Профіль безпеки</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/hcp/urothelial/uc-manage-adverse-events">Контроль побічних явищ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid section-menu hidden-sm-down">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav justify-content-center nav-fill">
                        <li class="nav-item has-dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Що таке Тецентрик&reg;?</a>
                            <ul class="dropdown-menu ">
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/nsclc-tecentriq-moa">Запропонований механізм дії</a>
                                </li>
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-tecentriq-dosing">Дозування</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="">Клінічні дослідження</a>
                            <ul class="dropdown-menu ">
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-identifying-tecentriq-patients">Кому підходить ТЕЦЕНТРИК&reg;</a>
                                </li>
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-imvigor-210-efficacy">Ефективність</a>
                                </li>
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-imvigor-210-study">Дизайн досліджень</a>
                                </li>
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-safety-profile">Профіль безпеки</a>
                                </li>
                                <li class="nav-item section-menu">
                                    <a class="nav-link" href="/hcp/urothelial/uc-manage-adverse-events">Контроль небажаних явищ</a>
                                </li>
                            </ul>
                        </li>
                    <!--    <li class="nav-item">
                            <a class="nav-link" href="#">Фінансова Підтримка Пацієнта</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Ресурсний Центр</a>
                        </li>  !-->
                    </ul>

                </div>
            </div>
        </div>
    </div>
    <?php
}
?>




<?
if ($slug == 'index') {
    echo Text::get('hcp-slider-index');
}
?>
<?
if ($slug != 'index') {
    ?>
    <div class="container">
        <div class="row">
            <?= $page->model->text ?>
        </div>
    </div>
    <?
}
?>


<?

if (isset($actual_link[2]) && $actual_link[2] == "lungs" && $slug != "index") {
    echo Text::get('hcp-lungs-footer');
}

if (isset($actual_link[2]) && $actual_link[2] == "urothelial" && $slug != "index") {
    if($slug == 'uc-identifying-tecentriq-patients'){

    } else {
        echo Text::get('hcp-urothelial-footer');
    }
    
}

if ($slug == "index") {
    echo Text::get('hcp-footer');
}
?>

