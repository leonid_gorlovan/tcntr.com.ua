<div class="container-fluid my-slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li class="active" data-target="#carouselExampleIndicators" data-slide-to="0">&nbsp;</li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1">&nbsp;</li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="carousel-item active"><a href="/hcp/nsclc-clinical-data-efficacy"><img class="d-block w-100" src="/images/slider/TecentrikA.png" alt="Тецентрик - розроблений для протистояння" /></a></div>

                        <div class="carousel-item"><a href="/hcp/nsclc-tecentriq-dosing"><img class="d-block w-100" src="/images/slider/TecentrikB.png" alt="Тецентрик: 1 фіксована доза 1 раз на 3 тижні" /></a></div>
                    </div>

                    <p><a class="carousel-control-prev" role="button" href="#carouselExampleIndicators" data-slide="prev"> <span class="sr-only">Назад</span> </a> <a class="carousel-control-next" role="button" href="#carouselExampleIndicators" data-slide="next"> <span class="sr-only">Вперед</span> </a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-4 sub-slider">
            <a href="/hcp/nsclc-tecentriq-moa"><img src="/images/proposed_moa_icon.png" alt="" /></a>
            <div class="sub-slider-block">
                <h3>МЕХАНІЗМ ДІЇ ПРЕПАРАТУ</h3>
                <p>Дізнайтеся, як Тецентрик <strong>&reg; </strong>відновлює протипухлинну активність імунних Т-клітин&nbsp;&nbsp;</p>
            </div>
        </div>

        <div class="col-lg-4 sub-slider">
            <a href="/hcp/nsclc-clinical-data-safety-profile"><img src="/images/patient_financial_support_icon.png" alt="" /></a>
            <div class="sub-slider-block">
                <h3>ПРОФІЛЬ БЕЗПЕКИ</h3>
                <p>Тецентрик <strong>&reg;:</strong> профіль безпеки при недрібноклітинному раку легень (НДКРЛ)</p>
            </div>
        </div>

        <div class="col-lg-4 sub-slider">
            <a href="/hcp/nsclc-clinical-data-manage-adverse-events"><img src="/images/register_icon.png" alt="" /></a>
            <div class="sub-slider-block">
                <h3>КОНТРОЛЬ БЕЗПЕКИ</h3>
                <p><a href="/hcp/nsclc-clinical-data-manage-adverse-events">Контроль застосування препарату</a> <a href="/hcp/nsclc-clinical-data-safety-profile">Тецентрик<strong>&reg;</strong></a></p>
            </div>
        </div>
    </div>
</div>
<br />
