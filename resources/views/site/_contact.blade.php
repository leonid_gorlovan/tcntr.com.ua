<div class="row">
    <div class="col-md-12">
        @if(!empty($alert))
            <div class="alert alert-success" role="alert">
                {{ $alert }}
            </div>
        @endif

        <div class="col-md-6">
            <div style="border-bottom:1px solid #ccc; padding-bottom: 15px;margin-bottom: 15px;">

                {!! Form::open(['url' => '/send-feedback', 'id' => 'contact-form']) !!}
                    <div class="form-group field-contactform-last_name required has-error">
                        <label class="control-label" for="contactform-last_name">Прізвище*</label>
                        {{ Form::text('last_name', null, ['id' => 'contactform-last_name', 'class' => 'form-control']) }}

                        @if ($errors->has('last_name'))
                            <span class="help-block help-block-error" role="alert">
                                {{ $errors->first('last_name') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group field-contactform-first_name required">
                        <label class="control-label" for="contactform-first_name">Ім'я*</label>
                        {{ Form::text('first_name', null, ['id' => 'contactform-first_name', 'class' => 'form-control']) }}

                        @if ($errors->has('first_name'))
                            <span class="help-block help-block-error" role="alert">
                                {{ $errors->first('first_name') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group field-contactform-city required">
                        <label class="control-label" for="contactform-city">Місто*</label>
                        {{ Form::text('city', null, ['id' => 'contactform-city', 'class' => 'form-control']) }}

                        @if ($errors->has('city'))
                            <span class="help-block help-block-error" role="alert">
                                {{ $errors->first('city') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group field-contactform-address required">
                        <label class="control-label" for="contactform-address">Адреса</label>
                        {{ Form::text('address', null, ['id' => 'contactform-address', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group field-contactform-email required">
                        <label class="control-label" for="contactform-email">E-Mail адреса*</label>
                        {{ Form::text('email', null, ['id' => 'contactform-email', 'class' => 'form-control']) }}

                        @if ($errors->has('email'))
                            <span class="help-block help-block-error" role="alert">
                                {{ $errors->first('email') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group field-contactform-phone required">
                        <label class="control-label" for="contactform-phone">Телефон</label>
                        {{ Form::text('phone', null, ['id' => 'contactform-phone', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group field-contactform-message required">
                        <label class="control-label" for="contactform-message">Ваше повідомлення*</label>
                        {{ Form::textarea('message', null, ['id' => 'contactform-message', 'class' => 'form-control', 'rows' => 6]) }}

                        @if ($errors->has('message'))
                            <span class="help-block help-block-error" role="alert">
                                {{ $errors->first('message') }}
                            </span>
                        @endif
                    </div>
                    <div class="form-group field-contactform-zgoda required">
                        <div class="checkbox">
                            <label for="myid">
                                <input type="checkbox" id="myid" name="zgoda" value="1">
                                Згода на обробку персональних даних
                            </label>
                            <br>
                            @if ($errors->has('zgoda'))
                                <span class="help-block help-block-error" role="alert">
                                    {{ $errors->first('zgoda') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary" name="contact-button">Відправити</button>
                    </div>

            {!! Form::close() !!}

            </div>
        </div>

        <div class="col-md-6"></div>
            <div style="padding-bottom: 20px; width: 100%;">
                <iframe width="100%" height="480" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.au/maps?f=q&amp;source=s_q&amp;hl=ru&amp;geocode=&amp;q=Sagaydachnogo 33, Kiev, Ukraine&amp;aq=&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=Sagaydachnogo 33, Kiev, Ukraine&amp;z=14&amp;output=embed"></iframe>
        </div>
    </div>
</div>
