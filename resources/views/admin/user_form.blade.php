@extends('adminlte::page')
@section('title', 'Юзеры')
@section('content_header')
<h1>Юзеры</h1>
@stop
@section('content')

    {!! Form::model($data, ['url' => '/admin/users/' . $type . '/save/' . object_get($data, 'id', 0), 'files' => true]) !!}

    <div class="box">
        <div class="box-header with-border">
            <a href="/admin/users/{{ $type }}" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
            <button type="submit" class="btn btn-success pull-right"><i class="far fa-save"></i> Сохранить</button>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Имя</label>
                {!! Form::text('name', null, ['class' => 'form-control form-control-sm']) !!}
            </div>
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Email</label>
                {!! Form::text('email', null, ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label><sup class="text-danger">*</sup> Пароль</label>
                        {!! Form::password('password', ['class' => 'form-control form-control-sm']) !!}
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group">
                        <label><sup class="text-danger">*</sup> Повт.-Пароль</label>
                        {!! Form::password('password_confirmation', ['class' => 'form-control form-control-sm']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

@stop
