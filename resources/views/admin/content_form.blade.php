@extends('adminlte::page')
@section('title', 'Контент')
@section('content_header')
<h1>Контент</h1>
@stop
@section('content')

    {!! Form::model($data, ['url' => '/admin/content/' . $type . '/save/' . object_get($data, 'id', 0), 'files' => true]) !!}

    <div class="box">
        <div class="box-header with-border">
            <a href="/admin/content/{{ $type }}" class="btn btn-primary"><i class="far fa-caret-square-left"></i></a>
            <button type="submit" class="btn btn-success pull-right"><i class="far fa-save"></i> Сохранить</button>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label><sup class="text-danger">*</sup> Название</label>
                {!! Form::text('title', null, ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="form-group">
                <label>Текст</label>
                {!! Form::textarea('text', null, ['class' => 'form-control form-control-sm', 'rows' => 8, 'id' => 'content', 'id' => 'content']) !!}
            </div>

            <p class="h3">SEO</p>

            <div class="form-group">
                <label>Seo H1</label>
                {!! Form::text('seo[h1]', object_get($data, 'seo.h1'), ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="form-group">
                <label>Seo Title</label>
                {!! Form::text('seo[title]', object_get($data, 'seo.title'), ['class' => 'form-control form-control-sm']) !!}
            </div>

            <div class="form-group">
                <label>Seo Keywords</label>
                {!! Form::textarea('seo[keywords]', object_get($data, 'seo.keywords'), ['class' => 'form-control form-control-sm', 'rows' => 4]) !!}
            </div>

            <div class="form-group">
                <label>Seo Description</label>
                {!! Form::textarea('seo[description]', object_get($data, 'seo.description'), ['class' => 'form-control form-control-sm', 'rows' => 4]) !!}
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-block btn-success">Сохранить</button>

    {!! Form::close() !!}

@stop

@section('js')

<script src="/vendor/tinymce/tinymce.min.js"></script>

<script>
    $(document).ready( function () {

        tinymce.init({
            selector: '#content',
            height : 550,
            plugins: 'table wordcount code',
            menu: {
                edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
                insert: {title: 'Insert', items: 'link media | template hr'},
                view: {title: 'View', items: 'visualaid'},
                format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
                table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                tools: {title: 'Tools', items: 'spellchecker code'}
            }
        });

    });
</script>

@stop
