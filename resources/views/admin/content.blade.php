@extends('adminlte::page')

@section('title', 'Контент')

@section('content_header')
    <h1>Контент</h1>
@stop

@section('content')
    <div class="box">
        {{--
        <div class="box-header with-border text-right">
            <a href="/admin/content/{{ $type }}/form" class="btn btn-primary"><i class="fas fa-plus"></i> Создать</a>

            @if($isArchive == 1)
                <a href="/admin/content/{{ $type }}" class="btn btn-warning"><i class="fas fa-list-ul"></i> Список</a>
            @else
                <a href="/admin/content/{{ $type }}/archive" class="btn btn-warning"><i class="fas fa-archive"></i> Архив</a>
            @endif
        </div>
        <!-- /.box-header --> --}}

        <div class="box-body">
            <table class="table table-md table-hover" id="table">
                <thead>
                    <th>Название</th>
                   {{-- <th class="edit"></th> --}}
                </thead>
            </table>
        </div>
    </div>
@stop

@section('js')

    <script>
        $(document).ready( function () {
            $('#table').DataTable({
                "language": {
                    "url": "/vendor/adminlte/plugins/dataTableRus.json"
                },
                serverSide: true,
                ajax: {
                    url: "/admin/content/{{ $type }}/json",
                    data: { isArchive: {{ $isArchive }} },
                    type: 'POST'
                },
                stateSave: true,
                "pageLength": 25,
                "aLengthMenu": [
                    [25, 50, 75, 100], [25, 50, 75, 100]
                ],
                "order": [
                    [ 0, "asc" ]
                ],
                "columns": [
                    { "data": "title" },
                    //{ "data": "" },
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "orderable": true,
                        "searchable": true,
                        mRender: function ( data, type, row ) {
                            return '<a href="/admin/content/{{ $type }}/form/' + row.id + '">' + data + '</a>';
                        }
                    },
                    // {
                    //     "targets": 1,
                    //     "orderable": false,
                    //     "searchable": false,
                    //     "width": "40px",
                    //     "render": function ( data, type, row ) {
                    //         if({{ $isArchive }})
                    //         {
                    //             return '<a href="/admin/content/{{ $type }}/restore/' + row.id + '" class="btn btn-success btn-xs"><i class="fa fa-undo"></i></a>';
                    //         }
                    //         else
                    //         {
                    //             return '<a href="/admin/content/{{ $type }}/delete/' + row.id + '" class="btn btn-warning btn-xs btn-archive"><i class="fa fa-archive"></i></a>';
                    //         }
                    //     }
                    // }
                ],
            });
        });
    </script>

@stop

