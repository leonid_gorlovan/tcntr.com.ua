<?php

return [
    'Login' => 'Авторизація',
    'E-Mail Address' => 'Email',
    'Password' => 'Пароль',
    'Confirm Password' => 'Пiд.-Пароль',
    'Remember Me' => 'Запам\'ятати мене',
    'Forgot Your Password?' => 'Забули свій пароль?',
    'Register' => 'Реєстрація',
    'Name' => 'Ім\'я',
    'Reset Password' => 'Скинути пароль',
    'Send Password Reset Link' => 'Надіслати пароль',
    'Verify Your Email Address' => 'Перевірте свою адресу електронної пошти',
    'A fresh verification link has been sent to your email address.' => 'З\'явилася нова перевірка посилання на вашу адресу електронної пошти.',
    'Before proceeding, please check your email for a verification link.' => 'Перш ніж продовжувати, будь ласка, перевірте свою електронну пошту для посилання на підтвердження.',
    'If you did not receive the email' => 'Якщо ви не отримали електронний лист',
    'click here to request another' => 'натисніть тут, щоб попросити іншого пароля'

];
