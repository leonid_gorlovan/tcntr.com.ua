<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', 'Site\ContentController@index');
    Route::get('/page/{slug}', 'Site\ContentController@page');
    Route::get('/hcp/{slug}', 'Site\ContentController@hcp');
    Route::post('/send-feedback', 'Site\ContentController@sendFeedback');

});

/************* admin zone ***********************/

Route::get('/admin/login', 'Admin\UserController@login');
Route::post('/admin/auth', 'Admin\UserController@auth');

Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function () {

    Route::get('/', function() {
        return view('admin.dashboard');
    });

    Route::group(['prefix' => 'content/{type}'], function () {
        Route::get('/', 'Admin\ContentController@index');
        Route::get('archive', 'Admin\ContentController@index');
        Route::post('/json', 'Admin\ContentController@json');
        Route::get('/form/{id?}', 'Admin\ContentController@form');
        Route::post('/save/{id?}', 'Admin\ContentController@save');
        Route::get('/delete/{id}', 'Admin\ContentController@delete');
        Route::get('/restore/{id}', 'Admin\ContentController@restore');
    });

    Route::group(['prefix' => 'users/{type}'], function () {
        Route::get('/', 'Admin\UserController@index');
        Route::get('archive', 'Admin\UserController@index');
        Route::post('/json', 'Admin\UserController@json');
        Route::get('/form/{id?}', 'Admin\UserController@form');
        Route::post('/save/{id?}', 'Admin\UserController@save');
        Route::get('/delete/{id}', 'Admin\UserController@delete');
        Route::get('/restore/{id}', 'Admin\UserController@restore');
    });

});
